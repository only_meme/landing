#!/bin/bash
if [[ "${DEBUG}" == "yes" ]]; then
	echo 'Setting DEBUG'
	set -exuo pipefail
else
	set -euo pipefail
fi

DOCKERFILE="${1}.docker"
IMAGE_TAG=${2}/${1}:${3}

buildah build -t "${IMAGE_TAG}" -f "${DOCKERFILE}" $(pwd)
podman push "${IMAGE_TAG}"
