from flask import render_template, flash, redirect, url_for
from . import main

@main.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

@main.route('/')
def home():
    return render_template('index.html')

@main.route('/account')
def account():
    return render_template('account.html')

@main.route('/privacy')
def privacy():
    return render_template('privacy.html')

@main.route('/terms')
def terms():
    return render_template('terms.html')

@main.route('/delete-me')
def deleteme():
    return render_template('delete-me.html')