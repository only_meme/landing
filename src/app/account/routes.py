from flask import Blueprint, request, redirect, url_for, session
from app.utils.keycloak_client import KeycloakClient

account = Blueprint('auth', __name__, url_prefix='/account')
keycloak_client = KeycloakClient()

@account.route('/register')
def register():
    # Redirect to Keycloak registration page
    registration_url = keycloak_client.get_registration_url()
    return redirect(registration_url)

@account.route('/login', methods=['POST'])
def login():
    username = request.form['username']
    password = request.form['password']
    tokens = keycloak_client.login(username, password)
    session['tokens'] = tokens
    return redirect(url_for('.dashboard'))

@account.route('/logout')
def logout():
    keycloak_client.logout(session['tokens']['refresh_token'])
    session.pop('tokens', None)
    return redirect(url_for('main.home'))

@account.route('/delete-account')
def delete_account():
    user_id = session['user_id']
    keycloak_client.delete_user(user_id)
    session.clear()
    return redirect(url_for('main.home'))

@account.route('/dashboard')
def dashboard():
    return "Welcome to your Dashboard"
