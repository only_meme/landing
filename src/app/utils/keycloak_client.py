from keycloak import KeycloakOpenID
from flask import current_app

class KeycloakClient:
    def __init__(self):
        self.keycloak_openid = KeycloakOpenID(
            server_url=current_app.config['KEYCLOAK_URL'],
            client_id=current_app.config['KEYCLOAK_CLIENT_ID'],
            realm_name=current_app.config['KEYCLOAK_REALM'],
            client_secret_key=current_app.config['KEYCLOAK_CLIENT_SECRET']
        )

    def get_registration_url(self):
        # Construct the registration URL
        return f"{self.keycloak_openid.auth_url()}?client_id={self.keycloak_openid.client_id}&response_type=code&scope=openid&redirect_uri={your_redirect_uri}&kc_action=register"

    def login(self, username, password):
        # Obtain tokens
        token = self.keycloak_openid.token(username, password)
        return token

    def logout(self, refresh_token):
        # Logout user
        self.keycloak_openid.logout(refresh_token)

    def delete_user(self, user_id):
        # Delete user
        self.keycloak_admin.delete_user(user_id)
