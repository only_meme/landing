from flask import Flask,render_template
from config import config_by_name
from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()

def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)  # Load configuration from the config class

    db.init_app(app)
    
    # Import blueprints
    from .main import main as main_blueprint
    from .account import main as account_blueprint
    # Register blueprints
    app.register_blueprint(main_blueprint)  
    app.register_blueprint(account_blueprint)

    return app

# Determine the configuration based on the environment variable
config_name = os.getenv('FLASK_CONFIG') or 'production'
app = create_app(config_by_name[config_name])

@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('404.html'), 404

if __name__ == '__main__':
    # Optionally, set up database tables if they don't exist (this should be done with caution in production)
    with app.app_context():
        db.create_all()

    if app.config['DEBUG'] or app.config['TESTING']:
    # Use the Flask development server
        app.run(host='0.0.0.0', port=5000)
    else:
        # Use Waitress in production
        from waitress import serve
        serve(app, host='0.0.0.0', port=5000)